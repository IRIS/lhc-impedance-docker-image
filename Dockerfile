FROM gitlab-registry.cern.ch/linuxsupport/cs9-base
LABEL author="konstantinos.paraschou@cern.ch"

RUN dnf -y install curl which bzip2 libgfortran git file man-db make \
                   gcc gcc-gfortran python3-pip python3-devel wget vim locmap-release

RUN dnf -y install locmap

RUN locmap --enable kerberos
RUN locmap --enable afs
RUN locmap --enable eosclient
RUN locmap --configure kerberos; exit 0
RUN locmap --configure afs; exit 0
RUN locmap --configure eosclient; exit 0

RUN dnf -y install eos-xrootd xrootd xrootd-client

WORKDIR /usr/local/lhc_pywit_model

RUN wget https://github.com/conda-forge/miniforge/releases/download/4.14.0-0/Miniforge3-4.14.0-0-Linux-x86_64.sh
RUN bash Miniforge3-4.14.0-0-Linux-x86_64.sh -b -p /usr/local/lhc_pywit_model/miniforge3
RUN rm Miniforge3-4.14.0-0-Linux-x86_64.sh

RUN source /usr/local/lhc_pywit_model/miniforge3/bin/activate && \
    conda init && \
    conda create --name lhc_pywit_model python=3.11 && \
    conda activate lhc_pywit_model && \
    conda update -n base -c conda-forge conda && \
    conda install pip && \
    conda install compilers=1.5.1

ENV PIP_ROOT_USER_ACTION=ignore

run source /usr/local/lhc_pywit_model/miniforge3/bin/activate && \
    conda activate lhc_pywit_model && \
    conda install -c conda-forge gsl mpfr arb cppyy   

run source /usr/local/lhc_pywit_model/miniforge3/bin/activate && \
    conda activate lhc_pywit_model && \
    git clone https://gitlab.cern.ch/IRIS/IW2D.git && \
    cd IW2D && \
    pip install -e .

run source /usr/local/lhc_pywit_model/miniforge3/bin/activate && \
    conda activate lhc_pywit_model && \
    cd IW2D/IW2D/cpp && \
    cp ./Makefile_system_GMP_MPFR Makefile && \
    make && \
    cd

run source /usr/local/lhc_pywit_model/miniforge3/bin/activate && \
    conda activate lhc_pywit_model && \
    pip install xobjects xdeps xtrack xpart xfields && \
    pip install xwakes && \
    python -c 'import xwakes; xwakes.initialize_pywit_directory()' && \
    cp IW2D/IW2D/cpp/flatchamber.x /root/pywit/IW2D/bin/. && \
    cp IW2D/IW2D/cpp/roundchamber.x /root/pywit/IW2D/bin/. && \
    cp IW2D/IW2D/cpp/wake_flatchamber.x /root/pywit/IW2D/bin/. && \
    cp IW2D/IW2D/cpp/wake_roundchamber.x /root/pywit/IW2D/bin/.

run source /usr/local/lhc_pywit_model/miniforge3/bin/activate && \
    conda activate lhc_pywit_model && \
    pip install scipy h5py numba pandas pytest
    

RUN echo "conda activate lhc_pywit_model" >> ~/.bashrc
